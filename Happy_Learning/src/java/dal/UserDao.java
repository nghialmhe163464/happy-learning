/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

/**
 *
 * @author Giang
 */
public class UserDao extends DBContext{
    public User getProfile(int userid){
        try {
            String sql = "Select * FROM [user] WHERE user_id = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userid);
            ResultSet rs = st.executeQuery();
            while(rs.next()){
                User u = new User();
                u.setUserid(rs.getInt("user_id"));
                u.setUsername(rs.getString("username"));
                u.setPassword(rs.getString("password"));
                u.setFullname(rs.getString("fullname"));
                u.setEmail(rs.getString("email"));
                u.setPhone_number(rs.getString("phone_number"));
                u.setSkill(rs.getString("skill_id"));
                u.setRole(rs.getString("role_id"));
                u.setBalance(rs.getString("balance"));
                u.setAvatar(rs.getString("avatar"));
                return u;
            }
        } catch (SQLException ex) {
            return null;
        }
        return null;
    }
    
    public void updateProfile(User u){
        try {
            String sql = "UPDATE [user] SET username = ?, password = ?, fullname = ?, phone_number = ?, avatar = ? WHERE user_id = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1,u.getUsername());
            st.setString(2,u.getPassword());
            st.setString(3,u.getFullname());
            st.setString(4, u.getPhone_number());
            st.setString(5,u.getAvatar());
            st.setInt(6, u.getUserid());
            ResultSet rs = st.executeQuery();
        } catch (Exception e) {
        }
    }
    
    public static void main(String[] args) {
        UserDao udao = new UserDao();
        //User u = new User(1, "giang", "123", "duong hoang giang", "", "", "", "", "", "", "0", "");
        //udao.updateProfile(u);
        System.out.println(udao.getProfile(1).getFullname());
    }
}
